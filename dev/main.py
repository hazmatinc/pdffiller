import json
import os

import sys
sys.path.insert(0, os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..', 'pdf_filler')))

from pdf_filler import PdfFiller, ORIGIN


def main():
    with open('coords.json') as data_file:
        coords_data = json.load(data_file)

    with open('value_mapping.json') as data_file:
        _key2text_map = json.load(data_file)

    key2text_map = {}
    key2text_map.update(_key2text_map)

    pf = PdfFiller('shipping_doc.pdf', coords_data, origin=ORIGIN.TOP_LEFT)

    # barcode_io = utils.generate_barcode_stream('upca', '123456789')
    # file handle StringIO needs to be converted to ImageReader to be processed
    #   by reportlab
    # http://stackoverflow.com/a/19160507/6553865
    # image = ImageReader(barcode_io)
    # pf.draw_image(image, 200, 200,
    #               width=100,  height=100, preserveAspectRatio=True)

    # pf.draw_text(130, 180, 'first name\nanother first name')
    # pf.draw_text(145, 210, 'last name')

    pf.fill(key2text_map, page=0)  # only the first page has this working atm
    pf.write('new.pdf')

    # from StringIO import StringIO
    # from shutil import copyfileobj
    # stream = StringIO()
    # pf.write_stream(stream)
    # with open('new.pdf', 'w') as fd:
    #     stream.seek(0)
    #     copyfileobj(stream, fd, -1)

    print 'Created pdf \'new.pdf\''

if __name__ == '__main__':
    main()
