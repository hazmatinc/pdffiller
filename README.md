# Installation #

generate with `python setup.py bdist_wheel`

`pip install dist/PdfFiller-0.1.1-py2.py3-none-any.whl`

`pip install -e git+https://bitbucket.org/hazmatinc/pdffiller/raw/master/dist/pdf_filler-0.1.1-py2.py3-none-any.whl#egg=pdf_filler`

# **Note:** #
Currently, only 1 pdf page can have text inserted at a time. Hoping to expand to multiple pages. Code is set to work on multiple pages. Just having issues to write it on PdfReader to have multiple pages.

Reportlab's coordinate system has origin at the bottom left corner

# Recording coordinate points from a pdf #
I used [gimp](https://www.gimp.org/downloads/)
if determine points on a pdf manually make sure it is being measure
    in pixel/pt or pt coordinate system.

Import PDF into gimp. Make sure it is using the letter format (8.5 in x 11 in). At the bottom of gimp make sure to set the measurement to point.
[here](https://gyazo.com/8c4f25c46fc1d0c2cddffd4f676d9bfc).

Now since gimp's origin is at the top left. We need to tell PdfFiller where the origin is at for the coordinate system we are using in gimp. `PdfFiller('old.pdf', {}, origin=ORIGIN.TOP_LEFT)`


# Example #
## Simple Text ##
### run example ###

Go to `../PdfFiller/examples/simple_text`

`python main.py`

Will output a new pdf file called `new.pdf` with mapping result values.
Main can be ran from where ever. It is using proper os pathing to resolve file paths.

PDF used: [application form](https://bitbucket.org/hazmatinc/pdffiller/src/a21d5467696a945f60d6ee3c3b79eee369f2a543/examples/simple_text/old.pdf?at=master&fileviewer=file-viewer-for-bitbucket-cloud%3Apdf-viewer)


**coords_data.json**

```
#!json
{
    "ei_name_last": {
        "coords": [62, 136],
        "font_size": 11,
        "align": "left"
    },
    "ei_name_first": {
        "coords": [218, 136],
        "font_size": 11,
        "align": "left"
    },
    "ei_name_middle": {
        "coords": [394, 136],
        "font_size": 11,
        "align": "left"
    }
}
```

### Keywords for coords_data json ###

`coords` - (list) position of value to be rendered at

`type` - (str) defaults to `text`. Whether mapping value is a string or file path for image. Choices: (`text` or `image`)

**type: `text`**

`font_size` - (int) defaults to 12

`font` - (str) defaults to Helvetica

`align` - (str) defaults to left. Choices: (`left`, `center`, `right`)

**type: `image`**

`width` - (int) defaults to None, which is original size

`height` - (int) defaults to None, which is original size

`preserveAspectRatio` - (bool) defaults to False

`anchor` - (str) defaults to `c`.


```
#!python

bitmap orientation
 nw  n  ne
 w   c  e
 sw  s  se
```


### Formatting

For `ei_name_last`. ei - employee information; name - the row title; last - the field I want to write onto specifically. For me this makes coming up with meaningful names and rarely need to worry about duplicate names.

**mapping_data.json**


```
#!json

{
    "ei_name_last": "Doe",
    "ei_name_first": "Jon",
    "ei_name_middle": ""
}
```
Pretty straight forward. If you want any hard values put it the json object and if you want dynamic values to modify `key2text_map`.

*example*

```
#!python

    key2text_map = {}
    key2text_map['ei_name_last'] = 'some other last name'
    key2text_map.update(_key2text_map)

```