from setuptools import find_packages, setup

setup(
    name="pdf_filler",
    version="0.1.2",
    install_requires=[
        'reportlab',
        'pyPdf',
        'aenum',
        # 'PyUPC-EAN',
    ],

    # metadata for upload to PyPI
    author="cdrandin",
    author_email="cdrandin@hotmail.com",
    description="Use coordinates to map text/images onto pdfs. WIP. Used for myself atm",
    license="PSF",
    keywords="PDF filler",
    url="",
    download_url="",
    # long_description="""
    # Uses coordinates to map text to pdfs.
    # Currently it only does text. Plan to expand to images in the future.
    # Only 1 page at a time is taken to account when mapping text to the PDF for now
    # Also plan on the future to make a 2nd portion of this software that will
    # help generate a coordinate file.
    # I use a mapper to map IDs with their associated values (text/image file).
    # """
)
