import imghdr
import os
from StringIO import StringIO

from pyPdf import PdfFileReader, PdfFileWriter
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas

# from ..utils.enums import ORIGIN
from enums import ORIGIN

# from aenum import Enum
#
# ORIGIN = Enum('ORIGIN', 'BOTTOM_LEFT TOP_LEFT TOP_RIGHT BOTTOM_RIGHT')


class PdfFiller(object):
    """
    Used for plugging in data onto the pdf.

    Notes:
        * all coordinates used in the mapping is relative to reportlab's origin
            change origin if coordinate system differs from reportlab

    IMPORTANT:
        * reportlab's coordinate system has origin at the bottom left corner
        * if determine points on a pdf manually make sure it is being measure
            in pixel/pt or pt coordinate system.
        * https://gyazo.com/a1770042fab178f136c38308bb08d02e
        * https://gyazo.com/e2bc992ec4b166caf6a52f2d8110e237
    """

    def __init__(self, pdf_file_path, coords_data=None, origin=ORIGIN.BOTTOM_LEFT):
        """
        Args:
            pdf_file_path (str): file path to pdf
            coords_data (dict): dictonary containing coordinate mapping info
            origin (enum.Enum): set origin of coordinate system used to when
                determining points on the pdf.
                different from that of reportlab (default: ORIGIN.BOTTOM_LEFT)
        """
        super(PdfFiller, self).__init__()
        self.pdf_file_path = pdf_file_path
        self.coords_data = coords_data if coords_data is not None else {}

        self.in_pdf = PdfFileReader(file(pdf_file_path, "rb"))
        self.origin = origin

        self.packet = StringIO()
        self.canvas = canvas.Canvas(self.packet, pagesize=letter)

    def dimension(self, page=0):
        """
        Returns:
            reportlab mediabox containing info on the rect of the pdf
        """
        return self.in_pdf.getPage(page).mediaBox

    def __offset_coords(self, x, y, page):
        """
        Should be used to get correct coordinate information for when offset
            coordinate system is being used

        Args:
            x (int): x value of coords
            y (int): y value of coords

        Returns:
            tuple of x and y minus their offset counterparts
        """
        new_coords = (int(x), int(y))

        if self.origin is ORIGIN.TOP_LEFT:
            new_coords = (x, self.dimension(page)[3] - y)
        elif self.origin is ORIGIN.TOP_RIGHT:
            new_coords = (self.dimension(page)[
                          2] - x, self.dimension(page)[3] - y)
        elif self.origin is ORIGIN.BOTTOM_RIGHT:
            new_coords = (self.dimension(page)[2] - x, y)

        return new_coords

    def __write_to_coords(self, key, value, page):
        """
        Args:
            key (str): key to coord specific meta data
            value (str): text or file path of image to render at coordinate
        """
        if key not in self.coords_data:
            return

        info = self.coords_data[key]
        # coords = self.__offset_coords(
        #     (info['coords'][0]), info['coords'][1], page)

        info.setdefault('font', 'Helvetica')
        info.setdefault('font_size', 12)
        info.setdefault('align', 'left')
        info.setdefault('type', 'text')

        x, y = (int(info['coords'][0]), int(info['coords'][1]))
        if info['type'].lower() == 'text':
            self.draw_text(x, y, value, info['font'], info['font_size'])
            # self.canvas.setFont(info['font'], info['font_size'])
            # self.draw_string(x, y, str(value), align=str(
            #     info['align']), mode=None)

        elif info['type'].lower() == 'image':
            width = int(info['width']) if 'width' in info else None
            height = int(info['height']) if 'height' in info else None
            mask = info['mask'] if 'mask' in info else None
            preserveAspectRatio = info[
                'preserveAspectRatio'] if 'preserveAspectRatio' in info else False
            anchor = str(info['anchor']) if 'anchor' in info else 'c'

            v = imghdr.what(value)
            if not v:
                raise Exception(
                    'key:{} | value:{} is not a valid image type'.format(key, value))

            self.draw_image(value, x, y,
                            width=width, height=height, mask=mask,
                            preserveAspectRatio=preserveAspectRatio,
                            anchor=anchor, page=page)

    def draw_string(self, x, y, text, align='left', mode=None, page=0):
        """
        Function same Canvas.DrawString/DrawCenteredString/DrawRightString,
            except that I just put it all into one method.

        Args:
            x (int): x position
            y (int): y position
            text (str): text to have rendered at (x, y) of pdf
            align (str): alignment; choices (left, center, right)
            mode (?): idk
            page (int): what page to draw onto. Defaults to 0.
            *** Currently doesn't work ***
        """

        (x, y) = self.__offset_coords(x, y, page)

        text = unicode(text).encode('utf-8')

        if align.lower() == 'right':
            self.canvas.drawCentredString(x, y, text, mode=mode)
        elif align.lower() == 'center':
            self.canvas.drawRightString(x, y, text, mode=mode)
        else:
            self.canvas.drawString(x, y, text, mode=mode)

    def draw_text(self, x, y, text, font='Helvetica', font_size=11, page=0):
        textobject = self.canvas.beginText()
        (x, y) = self.__offset_coords(x, y, page)
        textobject.setTextOrigin(x, y)
        textobject.setFont(font, font_size)

        text = unicode(text).encode('utf-8')

        lines = text.split('\n')
        for line in lines:
            textobject.textLine(line)

        self.canvas.drawText(textobject)

    def draw_image(self, image, x, y, width=None, height=None, mask=None,
                   preserveAspectRatio=False, anchor='c', page=0):
        """
        Same as Canvas.drawImage doc.

        Note:
            x and y position based off bottom left corner of image.
        """
        (x, y) = self.__offset_coords(x, y, page)
        self.canvas.drawImage(image, x, y,
                              width=width, height=height,
                              mask=mask,
                              preserveAspectRatio=preserveAspectRatio,
                              anchor=anchor)

    # def draw_upca_barcode(self, barcode_type, value, x, y, width=None, height=None, page=0):
    #     if len(value) is not 12:
    #         raise Exception("upc barcodes need to be 12 characters long.")
    #
    #     b = upcean.oopfuncs.barcode('upca', value)
    #     image = b.draw_barcode()
    #     image.seek(0)
    #     # (x, y) = self.__offset_coords(x, y, page)
    #
    #     # file handle StringIO needs to be converted to ImageReader to be
    #     # processed by reportlab
    #     # http://stackoverflow.com/a/19160507/6553865
    #     image = ImageReader(image)
    #     self.draw_image(image, x, y,
    #                     width=width, height=height, preserveAspectRatio=True)

    def fill(self, key2text_map, page=0):
        """
        Will map all keys containg text to appropriate coordinates

        Args:
            key2text_map (dict): mapping containing coordinates of where to
                map text to
        """

        if type(key2text_map) is not dict:
            raise TypeError("Expected dictionary")

        if page >= self.in_pdf.numPages:
            raise Exception(
                'Page ({}) index out of range. Should be within [0,{})'.format(
                    page, self.in_pdf.numPages))

        for k, v in key2text_map.items():
            self.__write_to_coords(k, v, page)

    def write(self, file_path):
        """
        Used to save changes made to pdf. Should only run once!

        Args:
            file_path (str): full path to where the pdf will be saved at
        """
        self.canvas.save()
        self.packet.seek(0)

        new_pdf = PdfFileReader(self.packet)
        output = PdfFileWriter()

        page = self.in_pdf.getPage(0)
        page.mergePage(new_pdf.getPage(0))
        output.addPage(page)

        # TODO: need to figure how to get more pages added on new pdf
        #   it can only do 1 page at a time.
        # for i, page in enumerate(self.in_pdf.pages):
        #     page.mergePage(new_pdf.getPage(0))
        #     output.addPage(page)

        if '.pdf' not in file_path:
            file_path = os.path.join(file_path, '.pdf')

        outputStream = file(file_path, "wb")
        output.write(outputStream)
        outputStream.close()

    def write_stream(self, stream):
        self.canvas.save()
        self.packet.seek(0)

        new_pdf = PdfFileReader(self.packet)
        output = PdfFileWriter()

        page = self.in_pdf.getPage(0)
        page.mergePage(new_pdf.getPage(0))
        output.addPage(page)

        # TODO: need to figure how to get more pages added on new pdf
        #   it can only do 1 page at a time.
        # for i, page in enumerate(self.in_pdf.pages):
        #     page.mergePage(new_pdf.getPage(0))
        #     output.addPage(page)

        output.write(stream)
        stream.seek(0)


def main():
    pf = PdfFiller('old.pdf', origin=ORIGIN.TOP_LEFT)

    # pf.draw_string(62, 136, 'last name')
    # pf.draw_string(218, 136, 'first name')
    # pf.draw_string(394, 136, 'middle name')
    # pf.draw_text(394, 136, 'another\nmiddle\nname', 'Helvetica', 11)

    # pf.write('new.pdf')

if __name__ == '__main__':
    main()
