import json
import os

from pdf_filler import PdfFiller, ORIGIN


def main():
    '''
    Currently this only works doing 1 pdf at a time and
    only able to insert text for the time being.
    '''
    pf = PdfFiller('old.pdf', origin=ORIGIN.TOP_LEFT)

    pf.draw_string(62, 136, 'last name')
    pf.draw_string(218, 136, 'first name')
    pf.draw_string(394, 136, 'middle name')

    pf.write('new.pdf')

if __name__ == '__main__':
    main()
