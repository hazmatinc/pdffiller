# from reportlab.lib.utils import ImageReader
#
# import upcean
# from PdfFiller.core.pdfFiller import PdfFiller
# from PdfFiller.utils.enums import ORIGIN
#
#
# def draw_upca_barcode(pf, barcode_type, value, x, y, width=None, height=None):
#     if len(value) is not 12:
#         raise Exception("upc barcodes need to be 12 characters long.")
#
#     b = upcean.oopfuncs.barcode('upca', value)
#     # get the Pil object or any image file handle object in order to
#     #   insert onto pdf
#     image = b.draw_barcode()
#     image.seek(0)
#
#     # file handle StringIO needs to be converted to ImageReader to be
#     # processed by reportlab
#     # http://stackoverflow.com/a/19160507/6553865
#     image = ImageReader(image)
#     pf.draw_image(image, x, y,
#                   width=width, height=height, preserveAspectRatio=True)
#
#
# def main():
#     '''
#     Currently this only works doing 1 pdf at a time and
#     only able to insert text for the time being.
#     '''
#     init = 100000000001
#     v = init
#     y_offset = 140
#
#     page = 0
#     while v < init + 50:
#         pf = PdfFiller('barcode_template.pdf', {}, origin=ORIGIN.TOP_LEFT)
#
#         # left column
#         draw_upca_barcode(pf, 'upca', str(v), 0, 175 +
#                           y_offset * 0, width=300, height=120)
#         v += 1
#         draw_upca_barcode(pf, 'upca', str(v), 0, 175 +
#                           y_offset * 1, width=300, height=120)
#         v += 1
#         draw_upca_barcode(pf, 'upca', str(v), 0, 175 +
#                           y_offset * 2, width=300, height=120)
#         v += 1
#         draw_upca_barcode(pf, 'upca', str(v), 0, 175 +
#                           y_offset * 3, width=300, height=120)
#         v += 1
#         draw_upca_barcode(pf, 'upca', str(v), 0, 175 +
#                           y_offset * 4, width=300, height=120)
#         v += 1
#
#         # right column
#         draw_upca_barcode(pf, 'upca', str(v), 320, 175 +
#                           y_offset * 0, width=300, height=120)
#         v += 1
#         draw_upca_barcode(pf, 'upca', str(v), 320, 175 +
#                           y_offset * 1, width=300, height=120)
#         v += 1
#         draw_upca_barcode(pf, 'upca', str(v), 320, 175 +
#                           y_offset * 2, width=300, height=120)
#         v += 1
#         draw_upca_barcode(pf, 'upca', str(v), 320, 175 +
#                           y_offset * 3, width=300, height=120)
#         v += 1
#         draw_upca_barcode(pf, 'upca', str(v), 320, 175 +
#                           y_offset * 4, width=300, height=120)
#         v += 1
#
#         pf.write('barcode_sheet_{}.pdf'.format(page))
#         page += 1
#
# if __name__ == '__main__':
#     main()
