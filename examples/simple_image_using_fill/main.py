import json
import os

from pdf_filler import PdfFiller, ORIGIN


def main():
    '''
    Currently this only works doing 1 pdf at a time and
    only able to insert text for the time being.
    '''
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           'coords_data.json')) as data_file:
        coords_data = json.load(data_file)

    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           'mapping_data.json')) as data_file:
        _key2text_map = json.load(data_file)

    key2text_map = {}
    # uncomment the below to override mapping_data. Mainly for dynamic content.
    # key2text_map['ei_name_last'] = 'last'
    # key2text_map['ei_name_first'] = 'first'
    # key2text_map['ei_name_middle'] = 'middle'
    key2text_map.update(_key2text_map)

    pf = PdfFiller('old.pdf', coords_data, origin=ORIGIN.TOP_LEFT)
    pf.fill(key2text_map, page=0)  # only the first page has this working atm
    pf.write('new.pdf')

if __name__ == '__main__':
    main()
