import os

from pdf_filler import PdfFiller, ORIGIN


def main():
    '''
    Currently this only works doing 1 pdf at a time and
    only able to insert text for the time being.
    '''
    pf = PdfFiller('old.pdf', {}, origin=ORIGIN.TOP_LEFT)

    image_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                              'wuah.jpg')

    # can be file path
    pf.draw_image(image_path, 200, 200,
                  # if width/height are None will use original width/height
                  width=150, height=200,
                  mask=None, preserveAspectRatio=True, anchor='c', page=0)

    # if ever using a file handler containing an image...
    # file handle needs to be converted to ImageReader to be
    # processed by reportlab
    # http://stackoverflow.com/a/19160507/6553865
    # use:
    # image = ImageReader(image_io)
    # pf.draw_image(image, x, y)

    pf.write('new.pdf')

if __name__ == '__main__':
    main()
